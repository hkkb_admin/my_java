package number;

public class TransferProperty {//定义类
int i=47;
public void call() {
	System.out.println("调用call方法");//定义成员方法，操作符调用
	for(i=0;i<3;i++) {
		System.out.print(i+" ");
		if(i==2) {
			System.out.println("\n");
		}
	}
}
public TransferProperty() {
}
public static void main(String[] args) {//下面开始在主方法里定义对象
	TransferProperty t1=new TransferProperty();//主方法中可以实例化一个对象，然后通过.操作符的调用成员变量的方法
	TransferProperty t2=new TransferProperty();
	t2.i=60;//将类变量赋值为60
	System.out.println("第一个实例调用对象i的结果: "+t1.i);//使用第一个对象调用类成员方法
	t1.call();
	System.out.println("第二个实例调用对象i的结果:"+t2.i);
	t2.call();
}
}
