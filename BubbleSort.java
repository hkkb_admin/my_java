package number;
public class BubbleSort {
	public static void main(String[] args) {
		int []array= {63,4,24,1,3,15};
		//BubbleSort sorter=new BubbleSort();
		//sorter.sort(array);
		
	}

	public void sort(int[] array) {
		for(int i=1;i<array.length;i++) {//控制比较次数
			for(int j=0;j<array.length-i;j++) {//内层循环主要用于控制比较相邻元素的大小
				if(array[j]<array[j+1]) {
				int temp=array[j];
				array[j]=array[j+1];
				array[j+1]=temp;
			}
			}
		}
		showArray(array);
	}
public static void showArray(int[] array) {
	for(int i:array) {
		System.out.print(">"+i);
	}
	System.out.println();
}
}
