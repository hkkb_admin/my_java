package number;
//在外部提供一个接口，在接口中声明一个方法
//实现该接口的内部类中实现该接口的方法
interface OutInterface{
	public void f();
}
public class interfaceInner{
	public static void main(String args[]) {
		OuterClass2 out =new OuterClass2();
		OutInterface outinter=out.doit();
		outinter.f();
	}
}
class OuterClass2{
	private class InnerClass implements OutInterface{//这个类中实现了不同的方法
		InnerClass(String s){
			System.out.println(s);
		}
		public void f() {
			System.out.println("访问内部的f（）构造方法");
		}
	}
	public OutInterface doit() {
		return new InnerClass("访问内部构造方法");
	}
}