package number;

public class AccessProperty {
	static int i=47;
	public void call() {
		System.out.println("调用call方法");
		for(i=0;i<3;i++) {
			System.out.print(i+"   ");
			if(i==2) {
				System.out.println("\n");
			}
		}
	}
	public AccessProperty() {//定义构造方法,要记得定义方法，不然对象无法创建
	}
public static void main(String[] args) {
	AccessProperty t1=new AccessProperty();
	AccessProperty t2=new AccessProperty();
	t1.i=60;//定义的是一类的成员变量，所以这里不管是t1还是t2i都是60
	System.out.println("第一个实例对象调用变量i的结果:  "+t1.i);
	t1.call();
	System.out.println("第二个实例对象调用变量i的结果:"+t2.i);//第二次调用后，变成了3
	t2.call();//输出后会显示
}
}
