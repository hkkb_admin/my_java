package number;

public class CreateObject1 {
	public static void main(String args[]) {
		Integer int1=new Integer(452);//封装对象
		int myint=int1.intValue();//Integer的valueOf（）就是把参数给的值，转化为Integer类型。
		System.out.println(int1);//以integer类型返回
	}

}
//Integer是int的包装类，int则是java的一种基本数据类型
//Integer变量必须实例化后才能使用，而int变量不需要
//Integer实际是对象的引用，当new一个Integer时，实际上是生成一个指针指向此对象；而int则是直接存储数据值
//Integer的默认值是null，int的默认值是0
