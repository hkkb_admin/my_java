package number;

public class Triangle {
	public float length1;
	public float length2;
	public float height;
	public Triangle(float length1,float length2,float height) {
		this.length1=length1;
		this.length2=length2;
		this.height=height;
		
	}
	public float square() {
		return (length1+length2)*height/2;
	}
public static void main(String args[]) {
	Triangle s=new Triangle(4.1f,4.4f,2.3f);
	System.out.println(s.square());
}
}
