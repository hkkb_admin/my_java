package com.lzw;

import java.util.Random;
import static java.lang.System.out;
public class FinalStaticData{
private static Random rand=new Random();//实例化一个随机对象
private final int a1=rand.nextInt(10);
private static final int a2=rand.nextInt(10);
public static void main(String args[]) {
	FinalStaticData fdata=new FinalStaticData();
	out.println("重写实例化对象调用a1的值："+fdata.a1);
	out.println("重写实例化对象调用a1的值："+fdata.a2);
	FinalStaticData fData2=new FinalStaticData();
	out.println("重新实例化对象调用a1的值："+fData2.a1);
	out.println("重新实例化对象调用a2的值："+fData2.a2);
}
	
}

