package number;
//继承，基于某个父类进行扩展，得到一个新的子类
public class Test {
	public Test() {//构造方法
		//somesentence
	}
protected void doSomething() {//成员方法
	//somesentence
}
protected Test dolt() {//返回值为Test类型的
	return new Test();
}
}
class Test2 extends Test{//继承
	public Test2() {
		super();
		super.doSomething();//子类调用父类方法
	}
	public void doSomething() {
		//SomeNewSentence
		
	}
	protected Test2 dolt() {
		return new Test2();//重写父类方法
	}
}