package number;

public class SelectSort {                     //选择排序算法
	public static void main(String args[]) {
		int[] array= {63,4,24,1,3,15};
		SelectSort sorter=new SelectSort();
		sorter.sort(array);    //创建sort函数
	}
public void sort(int[] array) {
	int index;
	for(int i=1;i<=array.length;i++) {
	     index=0;
		for(int j=1;j<=array.length-i;j++) {
			if(array[j]>array[index]) {               //在这里index代表着最大值，它来和array.length-i进行交换
				index=j;
			}
		}
		int temp=array[array.length-i];
		array[array.length-i]=array[index];
		array[index]=temp;
	}
	showArray(array);//创建array函数，定义在后面
}
	public void showArray(int[] array) {
		
		System.out.print(array[0]);           //因为之前是排好序的
	}

	
	
}

