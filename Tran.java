package number;
//一个用来检查参数受否小于0或大于100，如果参数小于0或大于100，则通过throw关键字抛出一个MyException对象，并在main方法中捕捉异常
public class Tran {
	static int avg(int number1,int number2) throws MyException{//有异常就抛出
		if(number1<0||number2<0) {
			throw new MyException("不可以使用负数");
			
		}
		if(number1>100||number2>100) {
			throw new MyException("数值太大了");
		}
		return (number1+number2)/2;
	}
	public static void main(String[] args) {
		try {
			int result=avg(-102,-150);
			System.out.println(result);
		}
		catch(MyException e) {
			System.out.println(e);
			
		}
	}
	
}
