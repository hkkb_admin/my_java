package number;

public class StaticTest {//这是类
	final static double PI=3.1415;//这两个都是在类中定义相关的变量
	static int id;
	public static void method1() {//定义非静态方法
		//do something
		
	}
	public void method2() {
		System.out.println(StaticTest.PI);//调用静态常量
		System.out.println(StaticTest.id);//调用静态变量
		StaticTest.method1();//静态方法里不能调用非静态方法与this关键字，不然会报错
	}

}
