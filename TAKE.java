package number;

public class TAKE {
	public static void main(String args[]) {
		try {//try里面包含的是可能出现异常的代码
		String str="lili";
		System.out.println(str+"年龄是：");
		int age=Integer.parseInt("20L");
		System.out.println(age);
		
		}catch(Exception e) {//catch语句捕获异常信息
			e.printStackTrace();
		}
		System.out.println("program over");//Java的异常处理是结构化的
		//会继续执行catch后的代码
		//getMessage():输出错误性质
		//toString()：给出异常的类型与性质
		//printStackTrace()：指出异常的类型、性质、栈层次以及出现在程序中的位置
	}

}
