package number;
public class Out {//外部类
    private String str="outStr";
    private void print()
    {
        System.out.println(str);
    }
    private class In{//内部类
        public String inStr="inStr";
        public void test()
        {
            System.out.println(str);
            print();
        }
    }
    public static void main(String[] args) {
        Out out=new Out();
        Out.In in=out.new In();
        in.test();
    }
}
//结果说明内部类可以任意访问外部类的成员函数成员变量，而外部类不能随意访问内部类的成员