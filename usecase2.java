package number;

public class usecase2 {
	public void doSomething(){//在父类中创建两个方法
		System.out.println("父类.doSomething()");
	}
	public void doAnything(){
		System.out.println("父类.doAnything()");
	}
	public static void main(String args[]){//在基类中调用这个方法
		usecase2 u=new sub();
		u.doSomething();
		u.doAnything();
	}
}
class sub extends usecase2{
	public void doAnything(){//在子类中覆盖了这个方法，并将它上传到基类中
		System.out.println("子类.doAnything()");
	}
}
