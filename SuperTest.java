package number;

public class SuperTest {
	 
	public static void main(String[] args) {
		
		new Child();		//孩子类无参构造
		new Child("我是孩子类带参构造方法");	//孩子类带参构造
	}
}
 
class Father{
 
	String money;
	
	public Father(){			//父亲类无参构造
		System.out.println("我是父亲类无参构造方法");
	}
	
	public Father(String t){	//父亲类带参构造
		System.out.println("我是父亲类带参构造方法");
	}
}
 
class Child extends Father{
	
	public Child(){				//孩子类无参构造        //没有new了
		//若方法体不显示调用父类的任何构造方法，则默认调用父类的空构造方法,也就是默认添加一句super();代码
		System.out.println("我是子类的无参构造方法");
	}
	
	public Child(String info){	//孩子类带参构造
		//若方法体不显示调用父类的任何构造方法，则默认调用父类的空构造方法,也就是默认添加一句super();代码
		System.out.println(info);
	}
	
	public void test(){
		
	}
}