package petShop;

public class Dog extends Pet{//继承Pet

	private String strain;//定义品种属性

	//两种构造方法
	public Dog() {
		// TODO Auto-generated constructor stub
	}
	
	public Dog(String name, int health, int love,String strain) {
		super(name, health, love);//super可以直接访问父类的构造方法
		this.strain = strain;
	}
	//get、set方法
	public String getStrain(){
		return strain;
	}
	public void setStrain(String strain) {
		this.strain = strain;
	}
	//重新父类中的print()方法
	@Override
	public void print() {
		// TODO Auto-generated method stub
		super.print();
		System.out.println("我是一只"+strain);
	}
	
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Dog d1 = new Dog("欧欧",100, 0, "雪纳瑞犬");//主方法里直接调用了
		d1.print();
	}

}
