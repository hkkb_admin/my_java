package number;

public class Rectangle {
	//尝试编写一个矩形类，将长与宽作为矩形类的属性
	//在构造方法中将长与宽初始化，定义一个成员方法求此矩形的面积
	public float length;
	public float width;
	public Rectangle(float length,float width) {//这是构造方法初始化
		            this.length=length;
		             this.width=width;                                                        //可以看到在构造方法中他将长与宽初始化了
		
		             
	}
	public float square() {
		return length*width;
	}
public static void main(String args[]) {//主函数定义一个成员方法来求
	Rectangle u=new Rectangle(3.5f,4.5f);
	System.out.println(u.square());
}
	
}
