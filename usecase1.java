package number;
//------------------------------------------------------------------------//
//程序目的，创建一个父类与子类，分别创建构造方法
public abstract class usecase1 {
	abstract void testAbstract();
	usecase1(){//（1）首先执行父类构造方法
		System.out.println("before testAbstract()");
		testAbstract();//如果调用了抽象方法，调用子类覆盖的方法。这里调用Atest类的testAbstract（）方法
		System.out.println("after testAbstarcat()");
	}
	public static void main(String args[]){
		new Atest();
	}
}
class Atest extends usecase1{//这里先把父类给继承了
	private int i=1;//（2）使成员变量进行初始化
	void testAbstract(){
		System.out.println("1111111111testAbstract()"+i);//这里给覆盖了一下
	}
	public Atest(){//（3）调用子类构造方法
		System.out.println(i);
	}
}
//很容易的可以发现，当创建子类对象的时候
//①先调用了子类的构造函数
//②调用了父类的构造函数
//③执行了父类的构造函数
//④执行了子类的构造函数
