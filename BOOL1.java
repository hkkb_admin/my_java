package number;

public class BOOL1 {
	public static void main(String args[]) {
		Boolean b=new Boolean("OK");//输入数据要用new——实例化对象，创建内存
		String str=b.toString();//通过字符串变量创建Boolean值，再将其转化为字符串输出
		System.out.println("OK"+str);
		b = new Boolean("true");
		str = b.toString();
		System.out.println("true:" + str);
	}

}
