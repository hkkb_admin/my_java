package number;

public class Compared {
	public static void main(String[] args) {
		String c1=new String("abc");
		String c2=new String("abc");
		String c3=c1;
		System.out.println("---------------------使用==运算符来比较c2与c3----------------------");
		System.out.println("比较结果为："+(c2==c3));
		System.out.println("---------------使用equals() 方法来比较---------------------");
		System.out.println("比较结果为："+(c2.equals(c3)));
	}
//者两种比较方法的不同之处在于equals比较的是String类中的方法，看两个比较对象指向的内容是否相等，而==则是比较这两对象所引用的地址是否相等
}
