package number;

public class UseCase3 {
	static class a{
		void f(){
			System.out.println("f()");//创建一个静态内部类，在主方法中创建其内部类的实例
		}
	}
	public static void main(String args[]){
		UseCase3.a a=new a();//不可以使用外部的非静态成员，不常用
		a.f();
	}
	
}