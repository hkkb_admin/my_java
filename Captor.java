package number;

public class Captor {
static int quotient(int x,int y) throws MyException{
	if(y<0) {
		throw new MyException("除数不能是负数");
		
	}
	return x/y;
}
public static void main(String args[]) {
	try {    
		int result=quotient(3,-1);
	}catch(MyException e) {//处理自定义异常
		System.out.println(e.getMessage());
	}catch(ArithmeticException e) {//算数异常
		System.out.println("除数不能为0");
	}catch(Exception e) {
		System.out.println("程序发生了其他异常");
	}
}
}
